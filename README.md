## PySpark-Boilerplate
A boilerplate for writing PySpark Jobs

For details see accompanyiong blog post at https://developerzen.com/best-practices-writing-production-grade-pyspark-jobs-cb688ac4d20f#.wg3iv4kie


## Execution d'un script

Localement :
````
spark-submit --py-files jobs.zip,libs.zip main.py --job wordcount
````

En prod :
````
spark-submit --py-files jobs.zip,libs.zip main.py --job wordcount
````



## Environnement 

demarrer zookeeper
````
bin/zookeeper-server-start.sh config/zookooeeper.properties 
````
demarrer kafka
````
bin/kafka-server-start.sh config/server.properties
````

Demarrer l'history server

````
/usr/local/spark/spark-2.4.0-bin-hadoop2.7/

./sbin/start-history-server.sh
````
